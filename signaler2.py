from aiohttp import web
import socketio,base64
from slugify import slugify
from pyee import AsyncIOEventEmitter
import logging
import  uuid,json
from twilio.rest import Client as TwilioClient
from twilio.base.exceptions import TwilioException, TwilioRestException

def time_ms():
    return int(time() * 1000)
import asyncio
from time import time

class MTimer:
    def __init__(self, timeout, callback, **kwargs):
        self._timeout = timeout
        self._callback = callback
        self._task = asyncio.create_task(self._run())
        self._kwargs = kwargs

    async def _run(self):
        await asyncio.sleep(self._timeout)
        await self._callback(**self._kwargs)

    def cancel(self):
        self._task.cancel()

sio = socketio.AsyncServer(cors_allowed_origins="*")
app = web.Application()
with open("config.json") as f:
    app.update(json.loads(f.read()))

sio.attach(app)



def generate_turn_creds(key):
    """Generate TURN server credentials for a client."""
    username,password = base64.b64decode(key).decode().split(":")
    return username, password

_chat_manager = None



def get_chat_manager():
    """Retrieve the global ChatManager object."""

    global _chat_manager
    if _chat_manager is None:
        _chat_manager = ChatManager()
    return _chat_manager

def sumar(x,y):
    return x+y
positionals={} 
data={}
distance=5
async def add_positional_client(room,client,position):
    manager = get_chat_manager()
    print("@@@@@")
    clients=manager.clients
    for client_id in clients:
        elem=clients[client_id]
        print("")
        print("tttttt ",elem.id,client.id)
        print("")
        if elem.id!=client.id:
            x=abs(position["x"]-elem.position[0])
            y=abs(position["y"]-elem.position[1])
            z=None
            if "z" in position:
                z=abs(position["z"]-elem.position[2])
            #Esto realmente funciona jajajaja
            #print("fffff",elem.id,client.id,client_id)
            if elem.room:
                print("IS INTO: ",
                    room,
                    x<elem.room.perimeter and y<elem.room.perimeter and ( True if z==None or z<elem.room.perimeter else False))
                if x<elem.room.perimeter and y<elem.room.perimeter and ( True if z==None or z<elem.room.perimeter else False):
                    #print("ggggg",elem.id,client_id)
                    """
                    try:
                        sio.enter_room(elem.id+"-room",sid)
                    except:
                        print("No existe la sala ",elem.id)
                    """
                    room.add_client(elem)
                    elem.room.add_client(client)

                    await manager.broadcast_room_info(room)
class ChatClient(AsyncIOEventEmitter):
    """A class that represents a connected client."""

    def __init__(self, id, username=None, room=None, is_admin=False):
        super().__init__()

        self.id = id
        self.username = username if username is not None else 'Major Tom2'
        print("uuuuuu",username)
        self.room = room
        self.is_admin = is_admin
        self.inbox = asyncio.Queue()
        self.outbox = asyncio.Queue()
        self._inbox_task = asyncio.create_task(
            self._process_inbox()
        )
        self.socket=None

        # Used by ChatManager for reaping
        # TODO: there is probably a cleaner solution
        self.last_seen = time_ms()
        self.timer = None
        self.position=[0,0,0]
        self.room=None#este seria la abstraccion de sala
        self.rooms=[]
        manager=get_chat_manager()
        manager.clients[self.id]=self

        logging.info('Created client %s', self.id)
    def set_position(self,position):
        if type(position)==dict:
            self.position[0]=position["x"]
            self.position[1]=position["y"]
            if "z" in position:
                self.position[2]=position["z"]
        else:
            self.position=position
    def add_room(self,room):
        self.rooms.append(room)


    async def _process_inbox(self):
        while True:
            message = await self.inbox.get()
            self.last_seen = time_ms()
            self.emit('message', message)
    
    async def send(self, data):
        """Send a message to the client."""
        """
        try:
            self.outbox.put_nowait(data)
            message_type = json.loads(data)['type']
            logging.info('Sent %s to client %s', message_type, self.id)

        except Exception as e:
            logging.info('Couldn\'t send message to client {}: {}'
                         .format(self.id, e))
        """
        d=json.loads(data)
        #print("######## enviando: ",d["type"])
        #print(json.dumps(d,indent=4,))
        await sio.emit("message",data,room=self.id,skip=self.id)
        
    async def ping(self):
        """Send the client a ping message."""

        message = ChatMessage()
        message.sender = 'ground control'
        message.receiver = self.id
        message.type = 'ping'
        message.data = time_ms()
        await self.send(message.json())

    async def shutdown(self):
        """Terminate the connection with the client."""

        message = ChatMessage()
        message.sender = 'ground control'
        message.receiver = self.id
        message.type = 'bye'
        message.data = time_ms()
        await self.send(message.json())

        if self.timer is not None:
            self.timer.cancel()
            self.timer = None

        if self._inbox_task is not None:
            self._inbox_task.cancel()
            self._inbox_task = None

        logging.info('Shut down client %s', self.id)


class ChatMessage:
    """A structured message that can be sent to a client."""

    def __init__(self, message=None):
        if isinstance(message, str):
            _json = json.loads(message)
        elif message is None:
            _json = {}
        else:
            _json = message

        # TODO: verify the sender against the client connection
        self.sender = _json.get('sender')
        self.receiver = _json.get('receiver')
        self.type = _json.get('type')
        self.data = _json.get('data')

    def json(self):
        """Get the JSON-encoded representation of the message."""
        return json.dumps(self.__dict__)

class ChatRoom(AsyncIOEventEmitter):
    """
    A class that tracks information for a specific room, including connected
    clients and room settings.
    """

    def __init__(self, name, password=None, guest_limit=None, admin_list=None, is_public=False):
        super().__init__()

        self.name = name
        self.id = slugify(name)
        self.clients = {}
        self.password_hash = None if password is None else generate_password_hash(password)
        self.guest_limit = guest_limit
        self.admin_list = admin_list if admin_list is not None else []
        self.is_public = is_public
        self._last_active = time_ms()
        self.perimeter=10
        self.position=[0,0,0]
        self.client=None# Este seria la abstraccion de cliente que este es
        self.headers={}

        def set_position(self,position):
            if type(position)==dict:
                self.position[0]=position["x"]
                self.position[1]=position["y"]
                self.position[2]=position["z"]
            else:
                self.position=position


        async def check_expire():
            """Check whether the room has expired.

            Rooms expire and are cleaned up after 60 minutes of inactivity.
            """

            now = time_ms() / 1000
            last_active = self.last_active / 1000

            if now - last_active > self._reap_timeout:
                logging.info('Room %s expired', self.id)
                self.emit('expire')
            else:
                new_timeout = last_active \
                    + self._reap_timeout - now

                self._timer = MTimer(new_timeout, check_expire)

        self._reap_timeout = 3600
        self._timer = MTimer(self._reap_timeout, check_expire)

        logging.info('Created room %s', self.id)


    @property
    def last_active(self):
        """A timestamp corresponding to when the room was last active."""

        last_seen = [client.last_seen for client in self.clients.values()]
        self._last_active = max([self._last_active, *last_seen])
        return self._last_active

    @property
    def active_ago(self):
        """The number of minutes ago that the room was last active."""
        return int((time_ms() - self.last_active) / 60000)

    @property
    def info(self):
        """
        Information about the room, consisting of the room ID and a list of
        connected clients.
        """

        clients = [{'id': client.id, 'username': client.username}
                   for client in self.get_clients()]
        """
        client = {'id': self.client.id, 'username': self.client.username}
        if clients not in clients:
            clients.append(client)
        """

        print("pppp",self.id,id(self),clients)
    
        return {'room_id': self.id, 'clients': clients}

    def authenticate(self, password=None):
        """Attempt to authenticate access to the room."""

        if password is None:
            return self.password_hash is None

        return check_password_hash(self.password_hash, password)

    def is_full(self):
        """Check whether the room's guest limit has been reached.

        Returns True if the guest limit has been reached, False otherwise.
        """

        return self.guest_limit is not None \
            and len(self.clients) == self.guest_limit

    def add_client(self, client):
        """Add a client to the room.

        Raises a ChatException if the room is already full.
        """

        if self.is_full():
            raise ChatException('Guest limit already reached')

        self.clients[client.id] = client
        client.room=self
        """
        if client not in client.rooms.clients:
            client.room.clients.append(self)
        """
    def remove_client(self, client):
        """Remove a client from the room."""

        logging.info(
            'Removing client {} from room {}'.format(
                client.id, self.id)
            )

        self._last_active = max(self._last_active, client.last_seen)
        client.room = None
        self.clients.pop(client.id, None)

        logging.info(
            '{} clients remaining in room {}'.format(
                len(self.clients), self.id)
            )

    def get_clients(self):
        """Get the clients connected to the room."""
        return self.clients.values()
    def get_client(self,id):

        for client in self.get_clients():
            if id==client.id:
                return client

    async def broadcast(self, message,event="message",headers={},block=True,skip=[]):
        """Send a message to all clients connected to the room."""
        """
        for client in self.get_clients():
            message.receiver = client.id
            client.send(message.json())
        """
        print("sssssssss",event,len(self.get_clients()))
        for client in self.get_clients():
            if client in skip:
                continue
            
            #client.send(message.json())
            if event=="message":
                message.receiver = client.id
                await sio.emit(
                event,message.json(),
                room=client.id)
            else:
             
                if headers and ((client.id in self.headers and headers!=self.headers[client.id]) or client.id not in self.headers):
                    headers["sender"]=self.id
                    headers["receiver"]=client.id
                    print("iiiiiiiiiiiiiii",event)
                    await sio.emit(
                    event,json.dumps(headers),
                    room=client.id)

                    if block:
                        self.headers[client.id]=headers

                elif not headers and self.headers[client.id]:
                    print("Buffer bloqueado porfavor vacia las headers de la sala")
                await sio.emit(
                event,message,
                room=client.id)

    async def shutdown(self):
        """
        Shut down the room.
        """

        if self._timer is not None:
            self._timer.cancel()
            self._timer = None

        for client in self.clients.values():
            self.remove_client(client)
            await client.shutdown()


class ChatManager:
    def __init__(self):
        self.rooms = {}
        self._message_address = "ground control"
        self._reap_timeout = 30
        self.clients={}



    async def _handle_message(self, message, client):
        chat_message = self._parse_message(message, client)
        print("========",chat_message.type, chat_message.receiver)
        if chat_message.receiver == self._message_address:
            print("*****")
            await self._handle_local_message(chat_message, client)
            return

        if chat_message.receiver == 'room':
            print("++++++")
            self._handle_room_message(chat_message, client)
            return

        if chat_message.receiver not in self.clients:
            print("-------")
            logging.info('Message recipient %s does not exist', chat_message.receiver)
            # TODO: reply with error
            return
        print("#####")
        to_client = self.clients[chat_message.receiver]
        await to_client.send(chat_message.json())

    async def _handle_local_message(self, message, client):
        manager=get_chat_manager()
        reply = ChatMessage()
        reply.sender = self._message_address
        reply.receiver = client.id
        
        if message.type == 'ping':
            reply.type = 'pong'
            reply.data = message.data
        elif message.type == 'pong':

            logging.info('Got pong {} from client {}'.format(message.data, message.sender))
            return
        elif message.type == 'profile':
            username = message.data.get('username')
            if username:
                client.username = username
            await self.broadcast_room_info(client.room)
            return
        elif message.type == 'get-room-info':
            #a partir de aqui es que se generan los pares 
       
            reply.type = 'room-info'
            print(manager.rooms)
            print("fffffffff ",client.id,client.username,client.room)
            reply.data = client.room.info
        elif message.type == 'update-data':
            reply.data=message.data
            reply.type="update-data"
            reply.data["client_id"]=client.id
        elif message.type == 'get-ice-servers':
            reply.type = 'ice-servers'
            reply.data = self.get_ice_servers(client.id)
        elif message.type == 'greeting':
            logging.info('Greeting received from client {}: {}'.format(message.sender, message.data))
            return
        elif message.type == 'bye':
            await self.remove_client(client)
            return
        else:
            reply.type = 'error'
            reply.data = 'Unknown message type: {}'.format(message.type)
        print("mmmmmmm")
        await client.send(reply.json())

    def _handle_room_message(self, message, client):
        room = client.room
        for c in room.clients.values():
            c.send(message.json())

    def _parse_message(self, message, client):
        chat_message = ChatMessage(message)
        if chat_message.sender is None:
            chat_message.sender = client.id
        return chat_message

    async def broadcast_room_info(self, room):
        """Send a room-info message to all clients in a given room."""

        message = ChatMessage()
        message.sender = self._message_address
        message.type = 'room-info'

        message.data = room.info
        await room.broadcast(message)

    def add_room(self, room):
        """Add a room."""
        self.rooms[room.id] = room

    async def remove_client(self, client):
        """Remove a client."""

        room = client.room
        if room:
            room.remove_client(client)
            await self.broadcast_room_info(room)

        await client.shutdown()

    async def _reap(self, client):
        # Ping client and allow time for a response
        # If a message is received in the meantime, this task is cancelled by the timer
        logging.info('Ping client {} pending reaping'.format(client.id))
        await client.ping()

        await asyncio.sleep(self._reap_timeout)
        await self.remove_client(client)

    def get_room(self, room_id):
        """Return a ChatRoom object given a room ID."""
        return self.rooms.get(room_id)

    def get_public_rooms(self):
        """Get a list of public rooms."""
        return sorted([room for room in self.rooms.values() if room.is_public],
                      key=lambda room: room.active_ago)

    def create_room(self, name,client=None, **kwargs):
        """Create a new room."""

        room_id = slugify(name)
        if room_id in self.rooms:
            return self.rooms[room_id]

        room = ChatRoom(name, **kwargs)
        client.room=room
        self.add_room(room)

        @room.on('expire')
        async def on_expire():
            await self.remove_room(room)
        
        return room

    async def remove_room(self, room):
        """Remove a room."""

        self.rooms.pop(room.id, None)
        await room.shutdown()
        logging.info('Removed room %s', room.id)

    async def create_client(self, client_id=None,username=None):
        """Create a new ChatClient."""

        if client_id is None:
            client_id = uuid.uuid4().hex
        
        #Esto lo omito de momento ya que igualmente las instancias 
        #de ChatClient no se guardan 
       
        if client_id in self.clients:
            #raise ChatException('Client {} already exists'.format(client_id))
            client=self.clients[client_id]
        else:
        
            client = ChatClient(client_id,username=username)
            
            client.timer = MTimer(self._reap_timeout, self._reap, client=client)

            greeting = ChatMessage()
            greeting.sender = self._message_address
            greeting.receiver = client.id
            greeting.type = 'greeting'
            greeting.data = 'This is Ground Control to Major Tom: You\'ve really made the grade. Now it\'s time to leave the capsule if you dare.'
            await client.send(greeting.json())

            @client.on("message")
            async def on_message(message):
                # Reap this client if we haven't seen it for too long
                if client.timer is not None:
                    client.timer.cancel()
                client.timer = MTimer(self._reap_timeout, self._reap, client=client)

                await self._handle_message(message, client)
        print("ddddd",client.id,client.username)
        return client

    def get_ice_servers(self, client_id):
        """Get a list of configured ICE servers."""
        print("xxxxxxxx")
        stun_host = app['STUN_HOST']
        stun_port = app['STUN_PORT']
        stun_url = 'stun:{}:{}'.format(stun_host, stun_port)
        servers = [{'urls': [stun_url]}]

        turn_host = app['TURN_HOST']
        turn_port = app['TURN_PORT']
        turn_key = app['TURN_STATIC_AUTH_SECRET']

        if turn_host and turn_port and turn_key:
            turn_url = 'turn:{}:{}'.format(turn_host, turn_port)
            username, password = generate_turn_creds(turn_key)
            servers.append({'urls': [turn_url], 'username': username, 'credential': password})

        servers = self.get_twilio_ice_servers()
        print("@@@@@@@@",servers)

        return servers

    def get_twilio_ice_servers(self):
        """Fetch a list of ICE servers provided by Twilio."""

        account_sid = app['TWILIO_ACCOUNT_SID']
        auth_token = app['TWILIO_AUTH_TOKEN']
        key_sid = app['TWILIO_KEY_SID']

        try:
            twilio = TwilioClient(key_sid, auth_token, account_sid)
            token = twilio.tokens.create()
            return token.ice_servers
        except (TwilioException, TwilioRestException):
            return []



positionals={} 
data={}
distance=5
def show_rooms(manager):
    print("ROOMS:")
    for room in manager.rooms.values():
        print(room)
        print("rooms: ",room.id,"clients: ",[[client.id,client.username] for client in room.clients.values()])
    print("")
    print("")
@sio.event
async def connect(sid,environ):
    from urllib.parse import urlparse, parse_qs

    manager = get_chat_manager()
    
    
    parsed_url = urlparse(environ["RAW_URI"])
    params=parse_qs(parsed_url.query)  
    position=json.loads(params["position"][0])
    username=params["username"][0]
    room=params["room"][0]
    print(params)
    client = await manager.create_client(sid,username=username)
    client.set_position(position)
    room = manager.create_room(room,client)
    if room is None:
        return  # close the websocket
    room.add_client(client)
    # sio.rooms(sid) #

    ##await add_positional_client(room,client,position)
    show_rooms(manager)
    await sio.emit("ready",room=sid)
    #await sio.emit("ready",room=sid, skip_sid=sid)
    #sio.enter_room(sid,)

@sio.event
def disconnect(sid):
    print("aaaaaaa")
    #sio.leave_room(sid,ROOM)

    if sid in positionals:
        del positionals[sid]
        for id in positionals:
            if sid in positionals[id]:
                positionals[id].remove(sid)
    #sio.leave_room(sid)
    print("Disconnected",sid)

@sio.event
async def data(sid,data):
    print("data from {}:".format(sid))
    for elem in positionals:
        dx=data["pos"][0] - player["pos"][0]
        dy=data["pos"][1] - player["pos"][1]
        dz=data["pos"][2] - player["pos"][2]
        if dx<=radio and dy<=radio and dz<=radio \
            and players<limiter:
            dataplayers.append(data["pos"])
            players+=1
    #await sio.emit("data",data,room=ROOM,skip_sid=sid)

@sio.event
async def message(sid,data):
    print(f"message from {sid}")
    manager = get_chat_manager()
    client=await manager.create_client(sid)
    room = manager.get_room(sid)

    client.emit("message",data)
    #await sio.emit("message",data,skip_sid=sid)
@sio.event
async def audio(sid,data):
    print("============ audio",sid)
    manager = get_chat_manager()
    client=await manager.create_client(sid)
    print(client.username,client.room)
    if client.room:
        await client.room.broadcast(data,"audio",{"data":{"id":client.username}},skip=[client])

    


import ssl
ssl_context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
#ssl_context.load_cert_chain(os.path.dirname(__file__)+'/signaler.crt', os.path.dirname(__file__)+'/signaler.key')
ssl_context.load_cert_chain('signaler.crt','signaler.key')
if __name__=="__main__":
    web.run_app(app,port=9999,ssl_context=ssl_context)
    #web.run_app(app,port=9999)
