
import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.websocket

sessions={}
from threading import RLock
import time,os,json

import ctypes
lock = RLock()
mydatabase={}
def get_db():
    return mydatabase

class WaveSourceFromData:
    def __init__(self, data):
        self.buffer=data
        self.channels = 1

        self.frequency = 24000
        #self.buffer = b"".join(data)
        self.buffer_length = len(self.buffer)
        

class OpusDecoderWS(tornado.websocket.WebSocketHandler):
    
    live_web_sockets = set()
    def initialize(self,database):
        self.database=database
        self.tiempo=time.time()

    def open(self):
       
        print('new connection')
        #self.set_nodelay(True)

        self.live_web_sockets.add(self)
        #self.write_message("you've been connected. Congratz.")

        self.initialized = False

    def my_init(self, data) :
        
        rate, is_encoded, op_rate, op_frm_dur,pos = data
        self.database[str(self._headers["Sec-Websocket-Accept"])]={"pos":pos}
        #rate : actual sampling rate
        #op_rate : the rate we told opus encoder
        #op_frm_dur : opus frame duration
        """
        self.filename = str(uuid.uuid4()) + '.wav'

        wave_write = wave.open(self.filename, 'wb')
        wave_write.setnchannels(1)
        wave_write.setsampwidth(2) #int16, even when not encoded
        wave_write.setframerate(rate)
        

        if self.initialized :
            self.wave_write.close()
        """


        self.frame_size = op_frm_dur * op_rate
        #self.wave_write = wave_write
        self.initialized = True
        
   
    @classmethod
    def send_message(cls, message,binary=False):
        removable = set()

        for ws in cls.live_web_sockets:
            if not ws.ws_connection or not ws.ws_connection.stream.socket:
                removable.add(ws)
            else:
                ws.write_message(message,binary=binary)

        for ws in removable:
            cls.live_web_sockets.remove(ws)
    

    def on_message(self, data) :
        radio=10
        limiter=10
        buffer=b""
        players=0
        dataplayers=[]
        if str(data).startswith('m:') :
            data=json.loads(str(data[len("m:"):]))
            if data["action"]=="init":
                #esto da igual aunque de momento lo voy a dejar
                self.my_init(data["data"])
            elif data["action"]=="update":

                mainSource=None
                stream=None
                for sess,player in self.database.items():
                    if str(sess)==str(self._headers["Sec-Websocket-Accept"]):
                        stream=player["stream"]

                with lock:
                    removable = set()
                    for socket in self.live_web_sockets:

                        if "pos" in player:
                        
                            dx=data["pos"][0] - player["pos"][0]
                            dy=data["pos"][1] - player["pos"][1]
                            dz=data["pos"][2] - player["pos"][2]
                            if dx<=radio and dy<=radio and dz<=radio \
                                and players<limiter:
                                dataplayers.append(data["pos"])
                                players+=1
                                
                                if str(self._headers["Sec-Websocket-Accept"])!=str(socket._headers["Sec-Websocket-Accept"]):
                                    if not socket.ws_connection or not socket.ws_connection.stream.socket:
                                        removable.add(socket)
                                    else:
                                        if stream:
                                            socket.write_message(json.dumps({"email":data["email"]}))
                                            socket.write_message(stream,binary=True)
                                        
                    #del self.database[self._headers["Sec-Websocket-Accept"]]["stream"]
                    for socket in removable:
                        self.live_web_sockets.remove(socket)
                        

                         

                                         
                    #actualiza el sonido 
                    #self.database[self._headers["Sec-Websocket-Accept"]]["sound"]=mainSource
                    #actualiza la posicion
                    
                    if str(self._headers["Sec-Websocket-Accept"]) in self.database.keys():
                        self.database[str(self._headers["Sec-Websocket-Accept"])]["pos"]=data["pos"]               
                    
                    self.database=self.database
                


        else:
            with lock:
                if str(self._headers["Sec-Websocket-Accept"]) in self.database:
                    self.database[str(self._headers["Sec-Websocket-Accept"])]["stream"]=data

                #aqui se podria hacer un cluster basado en cuadrantes e intermedios
             
                #self.wave_write.writeframes(data)

    def on_close(self):
        """
        if self.initialized :
            self.wave_write.close()
        """
        if str(self._headers["Sec-Websocket-Accept"]) in self.database:
            del self.database[str(self._headers["Sec-Websocket-Accept"])]
        print('connection closed')
    def check_origin(self, origin):
        allowed = ["http://localhost:3030", "http://localhost:8888"]
        if origin in allowed:
            print("allowed", origin)
            return 1

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("www/index.html")
mydatabase={}

app = tornado.web.Application([
    (r'/ws', OpusDecoderWS,dict(database=mydatabase)),
    (r'/', MainHandler),
    (r'/(.*)', tornado.web.StaticFileHandler, { 'path' : './www' })
])

http_server = tornado.httpserver.HTTPServer(app)
http_server.listen(int(os.environ.get('PORT', 8888)))
print('http server started')
tornado.ioloop.IOLoop.instance().start()
